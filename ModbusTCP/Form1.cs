﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MODBUS_TCP_Sample
{
    public partial class Form1 : Form
    {
        ModbusTCP myPLC;
        public Form1()
        {
            InitializeComponent();
            myPLC = new ModbusTCP(1);
            comboBox1.SelectedIndex = 0;

            List<int> myi = new List<int>();
            myi.Add(1); myi.Add(2); myi.Add(3); myi.Add(4); myi.Add(5);

            myPLC.WriteHoldRegister(0xE00, myi, 16);
            myPLC.WriteHoldRegister(0xEC8, myi, 32);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            List<byte> res = myPLC.ReadInputBits(0x400, 8);
            if (res != null)
            {
                Console.WriteLine("INPUTs:" + res[0].ToString("X2") + "  " + res[1].ToString("X2"));

                if ((res[1] & 0x01) == 0) { cb_X0.Checked = false; } else { cb_X0.Checked = true; }
                if ((res[1] & 0x02) == 0) { cb_X1.Checked = false; } else { cb_X1.Checked = true; }
                if ((res[1] & 0x04) == 0) { cb_X2.Checked = false; } else { cb_X2.Checked = true; }
                if ((res[1] & 0x08) == 0) { cb_X3.Checked = false; } else { cb_X3.Checked = true; }
                if ((res[1] & 0x10) == 0) { cb_X4.Checked = false; } else { cb_X4.Checked = true; }
                if ((res[1] & 0x20) == 0) { cb_X5.Checked = false; } else { cb_X5.Checked = true; }
                if ((res[1] & 0x40) == 0) { cb_X6.Checked = false; } else { cb_X6.Checked = true; }
                if ((res[1] & 0x80) == 0) { cb_X7.Checked = false; } else { cb_X7.Checked = true; }


            }

            res = myPLC.ReadOutputBits(0x500, 8);
            if (res != null)
            {
                if ((res[1] & 0x01) == 0) { cb_Y0.Checked = false; } else { cb_Y0.Checked = true; }
                if ((res[1] & 0x02) == 0) { cb_Y1.Checked = false; } else { cb_Y1.Checked = true; }
                if ((res[1] & 0x04) == 0) { cb_Y2.Checked = false; } else { cb_Y2.Checked = true; }
                if ((res[1] & 0x08) == 0) { cb_Y3.Checked = false; } else { cb_Y3.Checked = true; }
                if ((res[1] & 0x10) == 0) { cb_Y4.Checked = false; } else { cb_Y4.Checked = true; }
                if ((res[1] & 0x20) == 0) { cb_Y5.Checked = false; } else { cb_Y5.Checked = true; }
                if ((res[1] & 0x40) == 0) { cb_Y6.Checked = false; } else { cb_Y6.Checked = true; }
                if ((res[1] & 0x80) == 0) { cb_Y7.Checked = false; } else { cb_Y7.Checked = true; }
            }

            List<int> counters16bit = myPLC.ReadHoldRegister(0xE00);
            if (counters16bit != null)
            { 
                textBox1.Invoke((MethodInvoker)delegate () { textBox1.Text = counters16bit[0].ToString(); });
            }
            counters16bit = myPLC.ReadHoldRegister(0xEC8,10,32);
        }

        private void cb_INPUTs_CheckedChanged(object sender, EventArgs e)
        {
            var cb = (CheckBox)sender;
            if (cb.Checked)
                cb.BackColor = Color.LimeGreen;
            else
                cb.BackColor = Color.Salmon;

            cb.Invalidate();
        }

        private void cb_OUTPUTs_CheckedChanged(object sender, EventArgs e)
        {
            var cb = (CheckBox)sender;
            if (cb.Checked)
                cb.BackColor = Color.LimeGreen;
            else
                cb.BackColor = Color.Salmon;
            
            int address = 0x500 + ushort.Parse(cb.Text.Substring(1));
            myPLC.WriteOutputBits(Convert.ToUInt16(address & 0xffff), 0, cb.Checked);

            cb.Invalidate();
        }

        private void button_TX_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tb_Adress.Text) || string.IsNullOrEmpty(tb_Value.Text))
            {
                return;
            }

            int value, address, bits;

            try {
                int.TryParse(tb_Value.Text, out value);
                int.TryParse(tb_Adress.Text, out address);
                bits = comboBox1.SelectedIndex == 1 ? 32 : 16;
            }
            catch
            {
                return;
            }
        }
    }
}
