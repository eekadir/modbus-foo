﻿namespace MODBUS_TCP_Sample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gBox_Inputs = new System.Windows.Forms.GroupBox();
            this.tlp_Xs = new System.Windows.Forms.TableLayoutPanel();
            this.cb_X7 = new System.Windows.Forms.CheckBox();
            this.cb_X6 = new System.Windows.Forms.CheckBox();
            this.cb_X5 = new System.Windows.Forms.CheckBox();
            this.cb_X4 = new System.Windows.Forms.CheckBox();
            this.cb_X3 = new System.Windows.Forms.CheckBox();
            this.cb_X2 = new System.Windows.Forms.CheckBox();
            this.cb_X1 = new System.Windows.Forms.CheckBox();
            this.cb_X0 = new System.Windows.Forms.CheckBox();
            this.gBox_Outputs = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cb_Y7 = new System.Windows.Forms.CheckBox();
            this.cb_Y6 = new System.Windows.Forms.CheckBox();
            this.cb_Y5 = new System.Windows.Forms.CheckBox();
            this.cb_Y4 = new System.Windows.Forms.CheckBox();
            this.cb_Y3 = new System.Windows.Forms.CheckBox();
            this.cb_Y2 = new System.Windows.Forms.CheckBox();
            this.cb_Y1 = new System.Windows.Forms.CheckBox();
            this.cb_Y0 = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tb_Value = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_Adress = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button_TX = new System.Windows.Forms.Button();
            this.gBox_Inputs.SuspendLayout();
            this.tlp_Xs.SuspendLayout();
            this.gBox_Outputs.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // gBox_Inputs
            // 
            this.gBox_Inputs.Controls.Add(this.tlp_Xs);
            this.gBox_Inputs.Location = new System.Drawing.Point(12, 12);
            this.gBox_Inputs.Name = "gBox_Inputs";
            this.gBox_Inputs.Size = new System.Drawing.Size(93, 293);
            this.gBox_Inputs.TabIndex = 1;
            this.gBox_Inputs.TabStop = false;
            this.gBox_Inputs.Text = "INPUTs";
            // 
            // tlp_Xs
            // 
            this.tlp_Xs.ColumnCount = 1;
            this.tlp_Xs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_Xs.Controls.Add(this.cb_X7, 0, 7);
            this.tlp_Xs.Controls.Add(this.cb_X6, 0, 6);
            this.tlp_Xs.Controls.Add(this.cb_X5, 0, 5);
            this.tlp_Xs.Controls.Add(this.cb_X4, 0, 4);
            this.tlp_Xs.Controls.Add(this.cb_X3, 0, 3);
            this.tlp_Xs.Controls.Add(this.cb_X2, 0, 2);
            this.tlp_Xs.Controls.Add(this.cb_X1, 0, 1);
            this.tlp_Xs.Controls.Add(this.cb_X0, 0, 0);
            this.tlp_Xs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_Xs.Location = new System.Drawing.Point(3, 16);
            this.tlp_Xs.Name = "tlp_Xs";
            this.tlp_Xs.RowCount = 8;
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tlp_Xs.Size = new System.Drawing.Size(87, 274);
            this.tlp_Xs.TabIndex = 0;
            // 
            // cb_X7
            // 
            this.cb_X7.AutoSize = true;
            this.cb_X7.BackColor = System.Drawing.Color.Salmon;
            this.cb_X7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X7.Location = new System.Drawing.Point(3, 241);
            this.cb_X7.Name = "cb_X7";
            this.cb_X7.Size = new System.Drawing.Size(81, 30);
            this.cb_X7.TabIndex = 9;
            this.cb_X7.Text = "X7";
            this.cb_X7.UseVisualStyleBackColor = false;
            this.cb_X7.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // cb_X6
            // 
            this.cb_X6.AutoSize = true;
            this.cb_X6.BackColor = System.Drawing.Color.Salmon;
            this.cb_X6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X6.Location = new System.Drawing.Point(3, 207);
            this.cb_X6.Name = "cb_X6";
            this.cb_X6.Size = new System.Drawing.Size(81, 28);
            this.cb_X6.TabIndex = 8;
            this.cb_X6.Text = "X6";
            this.cb_X6.UseVisualStyleBackColor = false;
            this.cb_X6.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // cb_X5
            // 
            this.cb_X5.AutoSize = true;
            this.cb_X5.BackColor = System.Drawing.Color.Salmon;
            this.cb_X5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X5.Location = new System.Drawing.Point(3, 173);
            this.cb_X5.Name = "cb_X5";
            this.cb_X5.Size = new System.Drawing.Size(81, 28);
            this.cb_X5.TabIndex = 7;
            this.cb_X5.Text = "X5";
            this.cb_X5.UseVisualStyleBackColor = false;
            this.cb_X5.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // cb_X4
            // 
            this.cb_X4.AutoSize = true;
            this.cb_X4.BackColor = System.Drawing.Color.Salmon;
            this.cb_X4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X4.Location = new System.Drawing.Point(3, 139);
            this.cb_X4.Name = "cb_X4";
            this.cb_X4.Size = new System.Drawing.Size(81, 28);
            this.cb_X4.TabIndex = 6;
            this.cb_X4.Text = "X4";
            this.cb_X4.UseVisualStyleBackColor = false;
            this.cb_X4.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // cb_X3
            // 
            this.cb_X3.AutoSize = true;
            this.cb_X3.BackColor = System.Drawing.Color.Salmon;
            this.cb_X3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X3.Location = new System.Drawing.Point(3, 105);
            this.cb_X3.Name = "cb_X3";
            this.cb_X3.Size = new System.Drawing.Size(81, 28);
            this.cb_X3.TabIndex = 5;
            this.cb_X3.Text = "X3";
            this.cb_X3.UseVisualStyleBackColor = false;
            this.cb_X3.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // cb_X2
            // 
            this.cb_X2.AutoSize = true;
            this.cb_X2.BackColor = System.Drawing.Color.Salmon;
            this.cb_X2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X2.Location = new System.Drawing.Point(3, 71);
            this.cb_X2.Name = "cb_X2";
            this.cb_X2.Size = new System.Drawing.Size(81, 28);
            this.cb_X2.TabIndex = 4;
            this.cb_X2.Text = "X2";
            this.cb_X2.UseVisualStyleBackColor = false;
            this.cb_X2.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // cb_X1
            // 
            this.cb_X1.AutoSize = true;
            this.cb_X1.BackColor = System.Drawing.Color.Salmon;
            this.cb_X1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X1.Location = new System.Drawing.Point(3, 37);
            this.cb_X1.Name = "cb_X1";
            this.cb_X1.Size = new System.Drawing.Size(81, 28);
            this.cb_X1.TabIndex = 3;
            this.cb_X1.Text = "X1";
            this.cb_X1.UseVisualStyleBackColor = false;
            this.cb_X1.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // cb_X0
            // 
            this.cb_X0.AutoSize = true;
            this.cb_X0.BackColor = System.Drawing.Color.Salmon;
            this.cb_X0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_X0.Location = new System.Drawing.Point(3, 3);
            this.cb_X0.Name = "cb_X0";
            this.cb_X0.Size = new System.Drawing.Size(81, 28);
            this.cb_X0.TabIndex = 2;
            this.cb_X0.Text = "X0";
            this.cb_X0.UseVisualStyleBackColor = false;
            this.cb_X0.CheckedChanged += new System.EventHandler(this.cb_INPUTs_CheckedChanged);
            // 
            // gBox_Outputs
            // 
            this.gBox_Outputs.Controls.Add(this.tableLayoutPanel1);
            this.gBox_Outputs.Location = new System.Drawing.Point(190, 12);
            this.gBox_Outputs.Name = "gBox_Outputs";
            this.gBox_Outputs.Size = new System.Drawing.Size(93, 293);
            this.gBox_Outputs.TabIndex = 2;
            this.gBox_Outputs.TabStop = false;
            this.gBox_Outputs.Text = "OUTPUTs";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.cb_Y7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb_Y6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb_Y5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb_Y4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb_Y3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb_Y2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb_Y1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cb_Y0, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(87, 274);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cb_Y7
            // 
            this.cb_Y7.AutoSize = true;
            this.cb_Y7.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y7.Location = new System.Drawing.Point(3, 241);
            this.cb_Y7.Name = "cb_Y7";
            this.cb_Y7.Size = new System.Drawing.Size(81, 30);
            this.cb_Y7.TabIndex = 9;
            this.cb_Y7.Text = "Y7";
            this.cb_Y7.UseVisualStyleBackColor = false;
            this.cb_Y7.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // cb_Y6
            // 
            this.cb_Y6.AutoSize = true;
            this.cb_Y6.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y6.Location = new System.Drawing.Point(3, 207);
            this.cb_Y6.Name = "cb_Y6";
            this.cb_Y6.Size = new System.Drawing.Size(81, 28);
            this.cb_Y6.TabIndex = 8;
            this.cb_Y6.Text = "Y6";
            this.cb_Y6.UseVisualStyleBackColor = false;
            this.cb_Y6.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // cb_Y5
            // 
            this.cb_Y5.AutoSize = true;
            this.cb_Y5.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y5.Location = new System.Drawing.Point(3, 173);
            this.cb_Y5.Name = "cb_Y5";
            this.cb_Y5.Size = new System.Drawing.Size(81, 28);
            this.cb_Y5.TabIndex = 7;
            this.cb_Y5.Text = "Y5";
            this.cb_Y5.UseVisualStyleBackColor = false;
            this.cb_Y5.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // cb_Y4
            // 
            this.cb_Y4.AutoSize = true;
            this.cb_Y4.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y4.Location = new System.Drawing.Point(3, 139);
            this.cb_Y4.Name = "cb_Y4";
            this.cb_Y4.Size = new System.Drawing.Size(81, 28);
            this.cb_Y4.TabIndex = 6;
            this.cb_Y4.Text = "Y4";
            this.cb_Y4.UseVisualStyleBackColor = false;
            this.cb_Y4.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // cb_Y3
            // 
            this.cb_Y3.AutoSize = true;
            this.cb_Y3.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y3.Location = new System.Drawing.Point(3, 105);
            this.cb_Y3.Name = "cb_Y3";
            this.cb_Y3.Size = new System.Drawing.Size(81, 28);
            this.cb_Y3.TabIndex = 5;
            this.cb_Y3.Text = "Y3";
            this.cb_Y3.UseVisualStyleBackColor = false;
            this.cb_Y3.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // cb_Y2
            // 
            this.cb_Y2.AutoSize = true;
            this.cb_Y2.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y2.Location = new System.Drawing.Point(3, 71);
            this.cb_Y2.Name = "cb_Y2";
            this.cb_Y2.Size = new System.Drawing.Size(81, 28);
            this.cb_Y2.TabIndex = 4;
            this.cb_Y2.Text = "Y2";
            this.cb_Y2.UseVisualStyleBackColor = false;
            this.cb_Y2.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // cb_Y1
            // 
            this.cb_Y1.AutoSize = true;
            this.cb_Y1.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y1.Location = new System.Drawing.Point(3, 37);
            this.cb_Y1.Name = "cb_Y1";
            this.cb_Y1.Size = new System.Drawing.Size(81, 28);
            this.cb_Y1.TabIndex = 3;
            this.cb_Y1.Text = "Y1";
            this.cb_Y1.UseVisualStyleBackColor = false;
            this.cb_Y1.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // cb_Y0
            // 
            this.cb_Y0.AutoSize = true;
            this.cb_Y0.BackColor = System.Drawing.Color.Salmon;
            this.cb_Y0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_Y0.Location = new System.Drawing.Point(3, 3);
            this.cb_Y0.Name = "cb_Y0";
            this.cb_Y0.Size = new System.Drawing.Size(81, 28);
            this.cb_Y0.TabIndex = 2;
            this.cb_Y0.Text = "Y0";
            this.cb_Y0.UseVisualStyleBackColor = false;
            this.cb_Y0.CheckedChanged += new System.EventHandler(this.cb_OUTPUTs_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(416, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(79, 20);
            this.textBox1.TabIndex = 3;
            // 
            // tb_Value
            // 
            this.tb_Value.Location = new System.Drawing.Point(63, 23);
            this.tb_Value.Name = "tb_Value";
            this.tb_Value.Size = new System.Drawing.Size(54, 20);
            this.tb_Value.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tb_Value, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tb_Adress, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.comboBox1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.button_TX, 3, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(341, 257);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(241, 48);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Addres";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(63, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Value";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(123, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Bits";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tb_Adress
            // 
            this.tb_Adress.Location = new System.Drawing.Point(3, 23);
            this.tb_Adress.Name = "tb_Adress";
            this.tb_Adress.Size = new System.Drawing.Size(54, 20);
            this.tb_Adress.TabIndex = 8;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "16b",
            "32b"});
            this.comboBox1.Location = new System.Drawing.Point(123, 23);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(54, 21);
            this.comboBox1.TabIndex = 9;
            // 
            // button_TX
            // 
            this.button_TX.Location = new System.Drawing.Point(183, 23);
            this.button_TX.Name = "button_TX";
            this.button_TX.Size = new System.Drawing.Size(55, 22);
            this.button_TX.TabIndex = 10;
            this.button_TX.Text = "TX";
            this.button_TX.UseVisualStyleBackColor = true;
            this.button_TX.Click += new System.EventHandler(this.button_TX_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 367);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.gBox_Outputs);
            this.Controls.Add(this.gBox_Inputs);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gBox_Inputs.ResumeLayout(false);
            this.tlp_Xs.ResumeLayout(false);
            this.tlp_Xs.PerformLayout();
            this.gBox_Outputs.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox gBox_Inputs;
        private System.Windows.Forms.TableLayoutPanel tlp_Xs;
        private System.Windows.Forms.CheckBox cb_X7;
        private System.Windows.Forms.CheckBox cb_X6;
        private System.Windows.Forms.CheckBox cb_X5;
        private System.Windows.Forms.CheckBox cb_X4;
        private System.Windows.Forms.CheckBox cb_X3;
        private System.Windows.Forms.CheckBox cb_X2;
        private System.Windows.Forms.CheckBox cb_X1;
        private System.Windows.Forms.CheckBox cb_X0;
        private System.Windows.Forms.GroupBox gBox_Outputs;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox cb_Y7;
        private System.Windows.Forms.CheckBox cb_Y6;
        private System.Windows.Forms.CheckBox cb_Y5;
        private System.Windows.Forms.CheckBox cb_Y4;
        private System.Windows.Forms.CheckBox cb_Y3;
        private System.Windows.Forms.CheckBox cb_Y2;
        private System.Windows.Forms.CheckBox cb_Y1;
        private System.Windows.Forms.CheckBox cb_Y0;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tb_Value;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Adress;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button_TX;
    }
}

