﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MODBUS_TCP_Sample
{
    /*
     * No warranty no no no no
     * 
     * Just sample
     * 
     * 
     */
    class ModbusTCP
    {
        public ushort TransactionID;
        public ushort ProtocolID;
        public byte UnitID;
        public ModbusTCP(byte unit)
        {
            UnitID = unit;
        }

        public List<byte> PayloadAddHeader(List<byte> data)
        {
            List<byte> pack = new List<byte>();

            ushort length = Convert.ToUInt16(data.Count & 0xFFFF);

            pack.AddRange(BitConverter.GetBytes(TransactionID).Reverse());
            pack.AddRange(BitConverter.GetBytes(ProtocolID).Reverse());
            pack.AddRange(BitConverter.GetBytes(length).Reverse());
            pack.AddRange(data);

            return pack;
        }

        public List<byte> PayloadAddFuncParameter(byte function, ushort address, ushort length)
        {
            List<byte> pack = new List<byte>();

            pack.Add(UnitID);
            pack.Add(function);
            pack.AddRange(BitConverter.GetBytes(address).Reverse());
            switch (function)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                    pack.AddRange(BitConverter.GetBytes(length).Reverse());
                    break;
                case 5:
                    break;
                default:
                    break;
            }

            return pack;
        }
        public List<byte> ReadOutputBits(ushort address, ushort len)
        {
            List<byte> res = ExecuteFunc(1, address, len, null);
            if (res != null)
            {
                if (res[0] != UnitID) Console.WriteLine("Error : Unit ID is not equal");
                if ((res[1] & 0x80) == 0x80)
                {
                    Console.WriteLine("Error : Yapılmak istene işlem de hata oluştu");
                    return null;
                }
                res.RemoveRange(0, 2);
            }
            return res;
        }

        public List<byte> ReadInputBits(ushort address, ushort len)
        {
            List<byte> res = ExecuteFunc(2, address, len, null);
            if (res != null)
            {
                if (res[0] != UnitID) Console.WriteLine("Error : Unit ID is not equal");
                if ((res[1] & 0x80) == 0x80)
                {
                    Console.WriteLine("Error : Yapılmak istene işlem de hata oluştu");
                    return null;
                }
                res.RemoveRange(0, 2);
            }
            return res;
        }

        public List<int> ReadHoldRegister(ushort address, int count = 1, int bits = 16)
        {
            int bytes = bits / 8;
            List<int> registers = new List<int>();
            List<byte> res = ExecuteFunc(3, address, Convert.ToUInt16(count), null);
            if (res != null)
            {
                if (res[0] != UnitID) Console.WriteLine("Error : Unit ID is not equal");
                if ((res[1] & 0x80) == 0x80)
                {
                    Console.WriteLine("Error : Yapılmak istene işlem de hata oluştu");
                    return null;
                }
                res.RemoveRange(0, 2);
                if (res[0] == count * bytes)
                {
                    res.RemoveAt(0);

                    for (int n = 0; n < count * bytes; n += bytes)
                    {
                        int tmp = 0;
                        for (int i = 0; i < bytes; i++)
                        {
                            tmp <<= 8;
                            tmp |= res[n + i];
                        }
                        registers.Add(tmp);
                    }
                    return registers;
                }
            }
            return null;
        }

        public bool WriteHoldRegister(ushort address, List<int> values, int bits = 16)
        {
            int bytes = bits / 8;
            List<byte> txdata = new List<byte>();

            txdata.Add(Convert.ToByte((values.Count >> 8)& 0xff));
            txdata.Add(Convert.ToByte(values.Count & 0xff));
            txdata.Add(Convert.ToByte((values.Count * bytes) & 0xff));

            foreach (int value in values)
            {
                for(int i = 0; i < bytes; i++)
                {
                    txdata.Add(Convert.ToByte((value >> ((bytes-i-1)*8)) & 0xFF));
                }
            }
            List<byte> res = ExecuteFunc(16, address, Convert.ToUInt16(txdata.Count), txdata);
            if (res != null)
            {
                if (res[0] != UnitID) Console.WriteLine("Error : Unit ID is not equal");
                if ((res[1] & 0x80) == 0x80)
                {
                    Console.WriteLine("Error : Yapılmak istene işlem de hata oluştu");
                    return false;
                }

                return true;
            }
            return false;
        }

        public List<byte> WriteOutputBits(ushort address, ushort len, bool state)
        {
            List<byte> data = new List<byte>();
            if (state)
                data.Add(0xff);
            else
                data.Add(0);

            data.Add(0);

            List<byte> res = ExecuteFunc(5, address, len, data);
            if (res != null)
            {
                if (res[0] != UnitID) Console.WriteLine("Error : Unit ID is not equal");
                if ((res[1] & 0x80) == 0x80)
                {
                    Console.WriteLine("Error : Yapılmak istene işlem de hata oluştu");
                    return null;
                }
                res.RemoveRange(0, 2);
            }
            return res;
        }

        public List<byte> ExecuteFunc(byte func, ushort address, ushort length, List<byte> data)
        {
            byte[] msgRec = new byte[4096];
            int bytesRec, bytesSent;

            List<byte> message = new List<byte>();

            message.AddRange(PayloadAddFuncParameter(func, address, length));

            if (data != null)
                message.AddRange(data);

            message = PayloadAddHeader(message);
            try
            {

                IPAddress ipAddress = IPAddress.Parse("192.168.1.99");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 502);

                // Create a TCP/IP  socket.    
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.    
                try
                {
                    // Connect to Remote EndPoint  
                    sender.ReceiveTimeout = 300;
                    sender.Connect(remoteEP);

                    // Send the data through the socket.    
                    bytesSent = sender.Send(message.ToArray());

                    // Receive the response from the remote device.    
                    bytesRec = sender.Receive(msgRec);

                    // Release the socket.    
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                    return null;
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                    return null;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            if (bytesRec > 0)
            {
                List<byte> rec = new List<byte>();
                rec.AddRange(msgRec);
                rec.RemoveRange(bytesRec, msgRec.Length - bytesRec);

                ushort trid = Convert.ToUInt16((0x00FF & rec[0]) << 8 | 0x00FF & rec[1]);

                if (trid == TransactionID)
                {
                    rec.RemoveRange(0, 6); //remove transaction id and protocol id and lengt
                    TransactionID++;
                    return rec;
                }
            }

            return null;
        }
    }

    /*
    TIDx : 2B transaction id
    PIDx : 2B protocol id ( 0 for MODBUS/TCP)
    LENx : 2B length (number of remainin data)
    UID  : 1B unit id (255 if not used)
    FCD  : 1B function code
    DT   :  data

    [TID1] [TID0] [PID1] [PID0] [LEN1] [LEN0] [UID] [FCD] [DATA]

     1	Read Coil
     2	Read Discrete Input
     3	Read Holding Registers
     4	Read Input Registers
     5	Write Single Coil
     6	Write Single Holding Register
    15	Write Multiple Coils
    16	Write Multiple Holding Registers

                    DVP COM              MODBUS COM
    S bit       0x0000 - 0x03FF         1 - 1024
    X bit       0x0400 - 0x04ff         
    Y bit       0x0500 - 0x05ff
    T bit       0x0600 - 0x06ff
    T word      0x0600 - 0x06ff
    M bit       0x0800 - 0x0DFF
    M bit       0xB000 - 0xB9FF

    C word      0x0E00 - 0x0EC7
    C dword     0x0EC8 - 0x0EFF


*/
}
